import java.util.List;
import java.lang.Math;
        
public class AlgoDynamique {
    public int Tdes[][];
    public int Tpoi[][];
    public int size; 
    public Poids p;
    
    public AlgoDynamique(){}

    public void init(Poids p){
	this.p = p;
	size = p.poids.size();
	Tdes = new int[size][size];
	Tpoi = new int[size][size];
    }
    
    public void calculerpoids(Poids p){
	int l,c;
	for(l=0; l<size; l++){
	    Tpoi[l][l] = p.poids.get(l);
	    for(c=l+1;c<size; c++)
		Tpoi[l][c] = Tpoi[l][c-1]+p.poids.get(c);	    
	}
    }
    /**
     * Constrution de l'arbre de maniere recursif
     * @param l ligne dans le tableau
     * @param c colonne dans le tableau
     */
    public void afficherArbre(int l, int c){
	int k;
	int m;
	// Si c'est une feuille
	if (l==c)
	    System.out.print(p.poids.get(l));
	// Si ce n'est pas une feuille
	else {
	    // Recherche des premiers deux sous arbres
	    // qui ont donnés ce resultat
	    for(k=c-1; l<=k; k--){
                m = Math.abs(Tpoi[l][k] - Tpoi[k+1][c]) +
                             Tdes[l][k] + Tdes[k+1][c];
		if(m == Tdes[l][c]){
	                System.out.print("[");
              	  	afficherArbre(l,k);
                	System.out.print(",");
                	afficherArbre(k+1,c);
                	System.out.print("]");
			break;
     		}
	    }
        }
    }
    /**
     * Parcours du tableau en bottom-up
     */
    public int algoDynamique(){
	calculerpoids(p);
	int l,c,k;
	int m;
	// Parcours les colonnes
	for(c=0; c<size; c++){
	    Tdes[c][c] = 0;
	    // Parcours les lignes
	    for(l=c-1; 0<=l; l--){
		Tdes[l][c] = Math.abs(Tpoi[l][c-1] - Tpoi[c][c]) +
		                      Tdes[l][c-1] + Tdes[c][c];
		// Recherche du min
		for(k=c-1-1; l<=k; k--){
		    m = Math.abs(Tpoi[l][k] - Tpoi[k+1][c]) +
		                 Tdes[l][k] + Tdes[k+1][c];
		    if(m < Tdes[l][c]){
			Tdes[l][c] = m;
		    }
		}
	    }
	}
	return Tdes[0][size-1];
    }
}
