import java.util.List;
import java.util.ArrayList;

public class AlgoGlouton {
    public int Tpoi[];
    public Arbre abr;
    public int size;
    public Poids p;
    public List<Arbre> a;
    
    AlgoGlouton (){}
    
    public void init(Poids p){
	this.p = p;
	size = p.poids.size();
	Tpoi = new int[size];
        a = new ArrayList<Arbre>();
    }

        //Méthode permettant de trier les poids se trouvant dans un tableau
    public void triFusion(int tab[]){
        int longueur=tab.length;
        if (longueur>0)
            triFusion(tab,0,longueur-1);
    }
    
    public void triFusion(int tab[],int deb,int fin){
        if (deb!=fin){
            int milieu=(fin+deb)/2;
            triFusion(tab,deb,milieu);
            triFusion(tab,milieu+1,fin);
            fusion(tab,deb,milieu,fin);
        }
    }

	//Méthode permettant de fusionner les deux parties triées du tableau	
    public void fusion(int tab[],int deb1,int fin1,int fin2){
        int deb2 = fin1+1;
        int table1[] = new int[fin1-deb1+1];
        for(int i=deb1;i<=fin1;i++)
            table1[i-deb1] = tab[i];
        int compt1 = deb1;
        int compt2 = deb2;
        
        for(int i=deb1;i<=fin2;i++){
            if (compt1==deb2) break;
            else if(compt2==(fin2+1)){
                tab[i] = table1[compt1-deb1];
                compt1++;
            }
            else if (table1[compt1-deb1]<tab[compt2]){
                tab[i] = table1[compt1-deb1];
                compt1++;
            }
            else {
                tab[i] = tab[compt2];
                compt2++;
            }
        }
            
    }
    
	//Méthode qui remplit le tableau avec les poids donnés
    public void creerTab(Poids p){
        for(int i=0; i<size; i++)
            Tpoi[i] = p.poids.get(i);
    }
    
	//Méthode qui insère l'arbre créé dans la liste des arbres
    public void inserer(Arbre ab){
        int i;
	for(i=0; i<a.size(); i++){
		if(ab.poids<a.get(i).poids){
			a.add(i, ab);
			break;
		}	
	}
        if (i==a.size()) 
		a.add(ab);		
    }
    
	//Méthode principale de la classe
	/* Se charge de calculer les déséquilibres et les poids selon la version gloutonne souhaitée */
	/* Renvoie l'arbre obtenu après le traitement */
    public Arbre glouton(EnumAlgo ealgo){
	creerTab(p); 
	triFusion(Tpoi);
        int i;
        for(i=0;i<size;i++)
           a.add(i, new Feuille(Tpoi[i]));
        while(a.size()>1){
		if (ealgo == EnumAlgo.GLOUTON_CROISSANT){
            		Noeud n = new Noeud(a.get(0),a.get(1));
            		a.remove(1);
            		a.remove(0);    
            		inserer(n);	
		}
		else if (ealgo == EnumAlgo.GLOUTON_DECROISSANT){
			Noeud n = new Noeud(a.get(a.size()-1),a.get(a.size()-2));
            		a.remove(a.size()-1);
            		a.remove(a.size()-1);    
            		inserer(n);	
        	}
		else if (ealgo == EnumAlgo.GLOUTON_FIRSTLAST){
			Noeud n = new Noeud(a.get(0),a.get(a.size()-1));
            		a.remove(a.size()-1);
            		a.remove(0);    
            		inserer(n);
		}
	}
	
        return a.get(0);
     }
}

