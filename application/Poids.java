import java.io.*;
import java.lang.Math;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Poids{
    public List<Integer> poids;
    public Poids(){
	poids = new ArrayList<Integer>();
    }
    public void lireFichier(String fichier){
	String ligne;
	try{
	    InputStream is        = new FileInputStream(fichier); 
	    InputStreamReader isr = new InputStreamReader(is);
	    BufferedReader br     = new BufferedReader(isr);
	    while ((ligne = br.readLine())!=null){
		if (ligne.equals(""))
			break;
		poids.add(Integer.parseInt(ligne));
	    }
	    br.close(); 
	}		
	catch (Exception e){
	    System.out.println(e.toString());
	}
    }
    public void sauvegarder(String fichier){
	try {
	    FileWriter fw = new FileWriter (fichier);
	    BufferedWriter bw = new BufferedWriter (fw);
	    PrintWriter sortie = new PrintWriter (bw); 
	    for(int p:poids){
		sortie.println(p);
	    }
	    sortie.close();
	}
	catch (Exception e){
	    System.out.println(e.toString());
	}
    }
    public void generer(int quantite,int min,int max){
	int i;
	int value;
	for(i=0 ; i<quantite ; i++){
	    value = (int)(Math.ceil(Math.random()*max));
	    value = Math.max(min,value);
	    poids.add(value);
	}
    }
    public void generer(int quantite){
	generer(quantite,1,10);
    }
    public void lireCmd(){
	Scanner sc = new Scanner(System.in);
	int entree;
	System.out.println(
	    "Veuillez entrer votre séquence de poids\n"+
	    "-> Un poids est un entier naturel\n"+
            "-> Un poids par ligne, puis terminez par Ctrl+D"
        );
	while (sc.hasNext()){
	    try{
		entree = sc.nextInt();
		poids.add(entree);
	    }catch(Exception e){
		System.out.println("Ce n'est pas un entier naturel");
	    }
	    sc.nextLine();
        }
	sc.close();
    }
}
