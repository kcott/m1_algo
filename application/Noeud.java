public class Noeud extends Arbre {
public Arbre gauche;
public Arbre droit;

public Noeud(Arbre gauche, Arbre droit){
    this.gauche = gauche;
    this.droit = droit;
    setpoids();
    setdes();
    setdestotal();
}

//Méthode qui calcule le poids du noeud
public void setpoids(){
    poids = gauche.poids + droit.poids;
}

//Méthode qui calcule le déséquilibre du noeud
public void setdes(){
    des = Math.abs(gauche.poids - droit.poids);
}

//Méthode qui calcule le déséquilibre total du noeud
public void setdestotal(){
    destotal = des + gauche.destotal+droit.destotal;
}

//Méthode qui affiche l'arbre
public String toString(){
	 return ("["+gauche.toString()+","+droit.toString()+"]");
}
}
