public class GenererTest {
    
    public static void main(String []args){
	int i = 0;
	int min = 0;
	int max = 10;
	int nb = 0;
	String nameFile = "default";
        Poids p = new Poids();
	while (i<args.length){
	    if (args[i].equals("-min")){
		min = Integer.parseInt(args[i+1]);
		i = i+2;
	    }else if (args[i].equals("-max")){
		max = Integer.parseInt(args[i+1]);
		i = i+2;
	    }else if (args[i].equals("-nb")){
		nb = Integer.parseInt(args[i+1]);
		i = i+2;
	    }else{
		nameFile = args[i];
		i++;
	    }
	}
        p.generer(nb,min,max);
        p.sauvegarder(nameFile);
    }   

}
