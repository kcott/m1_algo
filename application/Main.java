import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;

public class Main{
    public static void main(String[] args,EnumAlgo eAlgo){
        boolean file    = false;
        boolean time    = false;
	int i           = 0;
	int desequilibre = 0;
	Arbre arbre = new Arbre();
	Poids p         = new Poids();
	String nameFile = "";
	String temps;
	long starttime=0, endtime=0, duration;

	// Lecture des arguments
	for (i=0; i<args.length; i++){
	    if (args[i].equals("-t")){
		    time = true;
		    i++;
		}else{
		    file = true;
		    nameFile = args[i];
		}
	}
	try {
	    // Traitement des poids
	    if (file)
		p.lireFichier(nameFile);
	    else
		p.lireCmd();

	    // Traitement de l'algo
	    if (eAlgo == EnumAlgo.DYNAMIQUE){
		starttime = System.currentTimeMillis();
		AlgoDynamique a = new AlgoDynamique();
		a.init(p);
		desequilibre = a.algoDynamique();
		// Affiche l'arbre de la premiere solution trouve
		a.afficherArbre(0,p.poids.size()-1);
		System.out.println();
		// Affiche le desequilibre de l'arbre
		System.out.println(desequilibre);
		endtime = System.currentTimeMillis();
	    }	
	    if (eAlgo == EnumAlgo.GLOUTON_CROISSANT){
		starttime = System.currentTimeMillis();
		AlgoGlouton a = new AlgoGlouton();
		a.init(p);
		arbre = a.glouton(eAlgo);
		System.out.println(arbre.toString());
		System.out.println(arbre.destotal);
		endtime = System.currentTimeMillis();
	    }
	    if (eAlgo == EnumAlgo.GLOUTON_DECROISSANT){
		starttime = System.currentTimeMillis();
		AlgoGlouton a = new AlgoGlouton();
		a.init(p);
		arbre = a.glouton(eAlgo);
		System.out.println(arbre.toString());
		System.out.println(arbre.destotal);
		endtime = System.currentTimeMillis();
	    }
		
	     if (eAlgo == EnumAlgo.GLOUTON_FIRSTLAST){
	     	starttime = System.currentTimeMillis();
	     	AlgoGlouton a = new AlgoGlouton();
		a.init(p);
		arbre = a.glouton(eAlgo);
		System.out.println(arbre.toString());
		System.out.println(arbre.destotal);
		endtime = System.currentTimeMillis();
	    }
	    // Affichage du temps
	    if (time){
		duration = endtime - starttime;
		temps = String.format(
				     "%d min, %d sec", 
				     TimeUnit.MILLISECONDS.toMinutes(duration),
				     TimeUnit.MILLISECONDS.toSeconds(duration) - 
				     TimeUnit.MINUTES.toSeconds(
                                     TimeUnit.MILLISECONDS.toMinutes(duration))
		);
		System.out.println(temps);
	    }
	} catch (Exception e) {
            System.out.println(e.toString());
	}
    }
}
